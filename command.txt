######################################
#Prepare data                       ## 
#                                   ##
######################################

# Align
# Image_size :182
# Command
python3 src/align/align_dataset_mtcnn.py train_data/CACD2000/CACD2000 train_data/CACD2000/CACD2000_224 --image_size 224 --margin 44

for N in {1..4}; do python3 src/align/align_dataset_mtcnn.py train_data/casia/casia_maxpy_mtcnnpy_182 train_data/casia/casia_maxpy_mtcnnalign_182 --image_size 182 --margin 44 --random_order --gpu_memory_fraction 0.2 & done

for N in {1..4}; do python3 src/align/align_dataset_mtcnn.py CASIA/CASIA-maxpy-clean/ train_data/CASIA/CASIA_new --image_size 224 --margin 20 --random_order --gpu_memory_fraction 0.22 & done

######################################
# Training                          ##
#                                   ##
######################################

# Run Inception_resnet_v1 Time/Epoch: 1.2
# train data: casia_maxpy_mtcnnalign_182
# learning_rate_schedule_file : learning_rate_schedule_classifier_casia.txt
# command
python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/casia/casia_maxpy_mtcnnalign_182 --image_size 160 --model_def models.inception_resnet_v1 --lfw_dir lfw/lfw_mtcnnalign_160 --optimizer RMSPROP --learning_rate -1 --max_nrof_epochs 80 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/learning_rate_schedule_classifier_casia.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9

# Run NN4 Time/epoch : 0.45
# train data: casia_maxpy_mtcnnalign_182
# learning_rate_schedule_file : learning_rate_schedule_classifier_casia.txt
# command 
python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/CACD2000/CACD2000_224 --image_size 96 --model_def models.nn4 --lfw_dir lfw/lfw_mtcnnpy_160 --optimizer ADAM --learning_rate -1 --max_nrof_epochs 200 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/lr_1.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9 --batch_size 64 --epoch_size 300 --gpu_memory_fraction 0.9

python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/CASIA/CASIA_dlib --image_size 96 --model_def models.nn4_small2_v1 --lfw_dir lfw/lfw_dlib_96/ --optimizer ADAM --learning_rate -1 --max_nrof_epochs 1000 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/learning_rate_schedule_classifier_casia.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9 --batch_size 96 --epoch_size 1000 

python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/CASIA/CASIA_dlib --image_size 96 --model_def models.nn4_small2_v1 --lfw_dir lfw/lfw_dlib_96/ --optimizer ADAM --learning_rate -1 --max_nrof_epochs 1000 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/learning_rate_schedule_classifier_casia.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9 --batch_size 96 --epoch_size 1000 --pretrained_model models/facenet/20170511-152915/


# Run NN3 Time/Epoch : 0.9
# train data: casia_maxpy_mtcnnalign_182
# learning_rate_schedule_file : learning_rate_schedule_classifier_casia.txt
# command
python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/casia/casia_maxpy_mtcnnalign_182 --image_size 160 --model_def models.nn3 --lfw_dir lfw/lfw_mtcnnalign_160 --optimizer RMSPROP --learning_rate -1 --max_nrof_epochs 80 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/learning_rate_schedule_classifier_casia.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9

# Run NN2 Time/Epoch : ??? Ko chay do data dau vao anh 182x182 trong khi image_size yeu cau la 224x224
# train data: casia_maxpy_mtcnnalign_182
# learning_rate_schedule_file : learning_rate_schedule_classifier_casia.txt
# command
python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/casia/casia_maxpy_mtcnnalign_182 --image_size 224 --model_def models.nn2 --lfw_dir lfw/lfw_mtcnnalign_160 --optimizer RMSPROP --learning_rate -1 --max_nrof_epochs 80 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/learning_rate_schedule_classifier_casia.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9


# VALIDATE
python3 src/validate_on_lfw.py lfw/lfw_mtcnnpy_160 20170216-091149 --lfw_pairs data/pairs.txt

 python3 src/validate_on_lfw.py lfw/lfw_mtcnnpy_96 models/facenet/20170427-035039 --lfw_pairs data/pairs.txt

sudo python3 src/compare.py models/facenet/20170428-034136/ lfw/lfw_mtcnnpy_96/Arnold_Palmer/Arnold_Palmer_0001.png lfw/lfw_mtcnnpy_96/Arnold_Palmer/Arnold_Palmer_0002.png --image_size 96



python3 src/facenet_train_classifier.py --logs_base_dir logs/facenet/ --models_base_dir models/facenet/ --data_dir train_data/CASIA/CASIA_224 --image_size 96 --model_def models.nn4 --optimizer ADAM --learning_rate -1 --max_nrof_epochs 1000 --keep_probability 0.8 --random_crop --random_flip --learning_rate_schedule_file data/lr_1.txt --weight_decay 5e-5 --center_loss_factor 1e-4 --center_loss_alfa 0.9 --batch_size 96 --epoch_size 4723
